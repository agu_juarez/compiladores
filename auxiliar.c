/*
	* File auxiliar containing the implementations of the functions defined in the file auxiliar.h
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "auxiliar.h"

struct nodoInformation *createnodoInformation(char *name, int value, int numberLine) {
	struct nodoInformation *datos = malloc(sizeof(struct nodoInformation));
	datos->name = name;
	datos->value = value;
	datos->numberLine = numberLine;

	return datos;	
}

struct nodoAST *createnodoAST(struct nodoInformation *data, char *label, struct nodoAST *hi, struct nodoAST *hd) {
	struct nodoAST *tree = malloc(sizeof(struct nodoAST));
	tree->inf = data;
	tree->label = label;
	tree->hi = hi;
	tree->hd = hd;

	return tree;
}

struct list *insert(struct list *lis, struct nodoInformation *data) {
	struct list *l = malloc(sizeof(struct list));
	l->inf = data;
	l->sig = lis;

	return l;
}

struct nodoInformation *searchNameVar(struct list *lis, char *name) {
	struct list *l = lis;

	while (l != NULL) {
		struct nodoInformation *inf = l->inf;

		if (strcmp(inf->name,name) == 0) {
			return inf;
		}

		l = l->sig;
	}

	return NULL;
}

int solve(struct nodoAST *n) {
	if (n != NULL) {
		if (strcmp(n->label, "Suma") == 0) {
			return solve(n->hi) + solve(n->hd);
		}
		if (strcmp(n->label, "Multiplicacion") == 0 ) {
			return solve(n->hi) * solve(n->hd);
		} 
		if (strcmp(n->label, "Variable") == 0) {
			return ((n->inf)->value);
		}
		if (strcmp(n->label, "Constante") == 0) 
			return ((n->inf)->value);
	}
}

void preOrder(struct nodoAST *n) {
	if (n != NULL) {
		if (strcmp(n->label, "Suma") == 0) {
			printf("%c", '+');
			preOrder(n->hi);
			preOrder(n->hd);
		}
		if (strcmp(n->label, "Multiplicacion") == 0 ) {
			printf("%c", '*');
			preOrder(n->hi);
			preOrder(n->hd);
		} 
		if (strcmp(n->label, "Variable") == 0) 
			printf("%s", (n->inf)->name);
		if (strcmp(n->label, "Constante") == 0)
			printf("%d", (n->inf)->value);
	}
}

