%{

#include <stdlib.h>
#include <stdio.h>

#include "types.h"
#include "auxiliar.h"

struct list *myList = NULL;

%}
 
%union {struct nodoInformation *p; struct nodoAST *n;}
 
%token<p> INT
%token<p> ID
%token    VAR

%type<n> expr
 
%left '+' 
%left '*'
 
%%
 
prog: expr ';'					{ printf("%s%d\n", "El resultado es: ", solve($1));}
    | decl expr ';'         	{ printf("%s%d\n", "El resultado es: ", solve($2));} 
    ;
  
decl: VAR ID '=' INT ';'	    { if (searchNameVar(myList, $2->name) != NULL) return printf("%s%d\n", "Variable duplicada en la linea " , $2->numberLine);
								  $2->value = $4->value;
								  myList = insert(myList, $2);}

	| decl VAR ID '=' INT ';'   { if (searchNameVar(myList, $3->name) != NULL) return printf("%s%d\n", "Variable duplicada en la linea " , $3->numberLine);
								  $3->value = $5->value;
		                          myList = insert(myList, $3);}
	;					


expr: INT               		{ $$ = createnodoAST($1, "Constante", NULL, NULL);}

	| ID  						{ if (searchNameVar(myList, $1->name) == NULL) return printf("%s%d\n", "Variable no declarada en la linea " , $1->numberLine);
								  $$ = createnodoAST(searchNameVar(myList, $1->name), "Variable", NULL, NULL);}
								
    | expr '+' expr     		{ $$ = createnodoAST(NULL, "Suma", $1, $3);}

    | expr '*' expr     		{ $$ = createnodoAST(NULL, "Multiplicacion", $1, $3);}

    | '(' expr ')'              { $$ =  $2;}
    ;
 
%%


