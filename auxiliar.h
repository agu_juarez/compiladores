/*
	* File containing headers of the functions relations a list and tree
*/

#ifndef __AUXILIAR__
#define __AUXILIAR__

#include "types.h"

/*
	* Function that create and return one pointer to nodoInformation
	* Parameters: *name, value to insert in the *name field of the node
	              value, value to insert in the value field of the node
	              numberLine, to insert in the value field of the node
	* Return: Poniter to new node nodoInformation
*/
struct nodoInformation *createnodoInformation(char *name, int value, int numberLine);

/*
	* Function that create and return one pointer to nodoAST
	* Parameters: *datos, node to which the inf field will point
				  *label, carry the node type
				  *hi
				  *hd
	* Return: Pointer to new node createnodoAST
*/
struct nodoAST *createnodoAST(struct nodoInformation *data, char *label, struct nodoAST *hi, struct nodoAST *hd);

/*
	* Function that insert one element to the list
	* Parameters: *lis, list in which *data is to be inserted
	              *datos, node to insert in *lis
	* Return: Pointer to new list containing the *data element
*/
struct list *insert(struct list *lis, struct nodoInformation *data);

/*
	* Function that search one variable in the list, return NULL if not exist and one pointer if exist
	* Parameters: *lis, list in which *name is to be searched
				  *name, name of the variable to search in *list
	* Return: Pointer to the corresponding node if *name exists and if NULL does not exist
*/
struct nodoInformation *searchNameVar(struct list *lis, char *name);


/*
	* Function interprete
	* Parameters: *n, tree father node
	* Return: Interger corresponding to the result of the tree analysis
*/
int solve(struct nodoAST *n);


/*
	* Function that travels the tree
	* Parameters: *n, tree father node
*/
void preOrder(struct nodoAST *n);

#endif