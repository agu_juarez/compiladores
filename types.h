/*
	* File containing structures for the list and the AST 
*/

#ifndef __TYPES__
#define __TYPES__

/*
	* Node to save information of the variables to be saved in the list
*/
typedef struct nodoInformation {
	char *name;
	int  value;
	int numberLine;
} nodoInformation;

/*
	* Node components of the list
*/
typedef struct list {
	nodoInformation *inf;
	struct list *sig;
} list;

/*
	* Node components of the AST
*/
typedef struct nodoAST {
	nodoInformation *inf;
	char *label;
	struct nodoAST *hi;
	struct nodoAST *hd;
} nodoAST;

#endif
