Primero: Clonar el repositorio con el comando "git clone https://agu_juarez@bitbucket.org/agu_juarez/compiladores.git"

Segundo: Abrir terminal dentro de la carpeta "compiladores"

Tercero: Compilar el script con el comando "sh script"

Cuarto: Ejecutar con el comando "./a.out ./Test/nombreArchivo.txt" donde nombreArchivo.txt puede ser inputFirst.txt, inputSecond.txt o inputThird.txt

Entradas: inputFirst.txt archivo el cual es correcto.
		  inputSecond.txt archivo en el cual la variable x esta duplicada.
		  inputThird.txt archivo en el cual la variable w no esta declarada.

Cuando se corre el comando: ./a.out ./Test/inputFirst.txt debe arrojar como resultado 55
							./a.out ./Test/inputSecond.txt debe arrojar "Variable duplicada en la linea 3"
							./a.out ./Test/inputThird.txt debe arrojar "Variable no declarada en la linea 4"